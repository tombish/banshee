<?php
	class dynamic_page_blocks extends Banshee\dynamic_blocks {
		protected $xslt_path = "views/demos";

		protected function timestamp() {
			$this->view->add_tag("time", date_string("j F Y, H:i:s"));
		}
	}
?>
