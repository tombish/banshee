$(document).ready(function() {
	$("input.datetimepicker").flatpickr({
		plugins: [
			ShortcutButtonsPlugin({
				button: [
					{
						label: "Yesterday"
					},
					{
						label: "Today"
					},
					{
						label: "Tomorrow"
					}
				],
				label: "or",
				onClick: (index, fp) => {
					let date;
					switch (index) {
						case 0:
							date = new Date(Date.now() - 24 * 60 * 60 * 1000);
							break;
						case 1:
							date = new Date();
							break;
						case 2:
							date = new Date(Date.now() + 24 * 60 * 60 * 1000);
							break;
					}
					fp.setDate(date);
				}
			})
		],
		dateFormat: "y-m-d",
		enableTime: true,
		locale: { firstDayOfWeek: 1 },
		weekNumbers: true,
	});
});
